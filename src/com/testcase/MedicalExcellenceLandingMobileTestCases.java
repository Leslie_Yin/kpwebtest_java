package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
//import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.Common;
//import com.dataprovider.TestDataProvider;
import com.pageobject.MedcicalExcellenceLandingPage;

public class MedicalExcellenceLandingMobileTestCases {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String environment = null;
	public static String platform = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String url = null;
	private MedcicalExcellenceLandingPage landingpage = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("previewprod") String _environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		environment = _environment;
		
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "platform", "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Windows") String _platform, @Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("360") String _width, @Optional("640") String _height) throws Exception {
		platform = _platform;
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(platform, browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
	}

	@Parameters({ "url" })
	@BeforeMethod
	public void beforeMethod(@Optional("http://preview.thrive.prod.dpaprod.kpwpce.kp-aws-cloud.org/healthier-outcomes") String _url) throws Exception {
		url = _url;
		driver.navigate().to(url);
		if(environment.equals("proferochina")){
			ShareSteps.getAuthorization(driver, platform);
		}
		Thread.sleep(10000);
		landingpage = new MedcicalExcellenceLandingPage(driver);
	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_MedicalExcellenceLandingPage_PageLoadMobile() throws Exception {
		try {
			Common.scrollTo(0);
			Thread.sleep(1000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-PageLoadMobile");
			Assert.assertTrue( !driver.getTitle().contains("Page not found"), "Page not found");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_MedicalExcellenceLandingPage_ClickMobileMenu() throws Exception {
		try {			
			landingpage.getMobileMenu().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickMobileMenu");
			landingpage.getMobileMenu().click();
			Thread.sleep(1000);
			
			Assert.assertTrue( landingpage.getMobileMenu().isEnabled(), "MobileMenu is enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_MedicalExcellenceLandingPage_ClickAnchorButtonsMobile() throws Exception {
		try {
			Common.scrollTo(landingpage.getCareStoriesThumbnails().get(1).getLocation().getY()-200);
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-AnchorButtonsMobile");
			
			for(int i=0;i<landingpage.getArchorButtons().size();i++){
				landingpage.getArchorButtons().get(i).click();
				Thread.sleep(1000);
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-ClickedAnchorButtonsMobile-"+landingpage.getArchorButtons().get(i).getText());
			}
			Assert.assertTrue( 3 == landingpage.getArchorButtons().size(), "ArchorButtons count is not 3");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_MedicalExcellenceLandingPage_ClickCareStoriesThumbnailsMobile() throws Exception {
		try {
			Common.scrollTo(landingpage.getCareStoriesThumbnails().get(1).getLocation().getY()-20);
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CareStoriesThumbnailsMobile");
			
			String thumbnailname = null;
			for(int i=0;i<landingpage.getCareStoriesThumbnails().size();i++){
				int index = ((i+1)>=landingpage.getCareStoriesThumbnails().size())?0:(i+1);
				Common.scrollTo(landingpage.getCareStoriesThumbnails().get(index).getLocation().getY());
				thumbnailname = landingpage.getCareStoriesThumbnails().get(index).findElement(By.className("text")).findElement(By.tagName("span")).getText();
				landingpage.getCareStoriesThumbnails().get(index).click();
				Common.switchBackToInitialTab(driver);
				Thread.sleep(1000);
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-ClickedAnchorButtonsMobile-" + thumbnailname);
			}
			Assert.assertTrue( 4 == landingpage.getCareStoriesThumbnails().size(), "CareStoriesThumbnails count is not 4");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
