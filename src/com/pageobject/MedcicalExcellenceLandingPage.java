package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class MedcicalExcellenceLandingPage {
public WebDriver driver = null;
	
	public MedcicalExcellenceLandingPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "li.selected > a.link-category-section")
	private WebElement selectedMenu;
	public WebElement getSelectedMenu(){
		return selectedMenu;
	}
	
	@FindBys({
		@FindBy(id = "submenu-content-4"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> selectedMenuNavigationLinks;
	public List<WebElement> getSelectedMenuNavigationLinks(){
		return selectedMenuNavigationLinks;
	}
	
	@FindBy(className = "mobile-menu")
	private WebElement mobileMenu;
	public WebElement getMobileMenu(){
		return mobileMenu;
	}
	
	@FindBys({
		@FindBy(className = "js-anchors"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> archorButtons;
	public List<WebElement> getArchorButtons(){
		return archorButtons;
	}
	
	@FindBy(className = "thumbnail")
	private List<WebElement> careStoriesThumbnails;
	public List<WebElement> getCareStoriesThumbnails(){
		return careStoriesThumbnails;
	}
	
	@FindBys({
		@FindBy(className = "list"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> spotlightSpecialtiesLinks;
	public List<WebElement> getSpotlightSpecialtiesLinks(){
		return spotlightSpecialtiesLinks;
	}
	
	@FindBy(className = "tile-thumbnails")
	private List<WebElement> regionLinks;
	public List<WebElement> getRegionLinks(){
		return regionLinks;
	}
}
